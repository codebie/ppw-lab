from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_get_client_id(self):
        self.assertEqual(CSUIhelper().instance.get_client_id(), 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

    def test_get_auth_param_dict(self):
        dic = CSUIhelper().instance.get_auth_param_dict()
        self.assertEqual(dic['client_id'], CSUIhelper().instance.get_client_id())

    def test_lab_7_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend(self):
        Client().post('/lab-7/add-friend/', {'name':'windi', 'npm':'123'})
        self.assertEqual(Friend.objects.all().count(), 1)

    def test_add__friend_duplicate(self):
        Client().post('/lab-7/add-friend/', {'name':'windi', 'npm':'123'})
        Client().post('/lab-7/add-friend/', {'name':'windi', 'npm':'123'})
        self.assertEqual(Friend.objects.all().count(), 1)

    def test_delete_friend(self):
        Client().post('/lab-7/add-friend/', {'name':'windi', 'npm':'123'})
        Client().post('/lab-7/delete-friend/', {'id':1})
        self.assertEqual(Friend.objects.all().count(), 0)

    def test_validate_npm(self):
        Client().post('/lab-7/add-friend/', {'name':'windi', 'npm': '123'})
        response = Client().post('/lab-7/validate-npm/', {'npm':'123'})
        self.assertIn('true',response.content.decode('utf8'))

    def test_friend_list_exist(self):
        response = Client().post('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_friend_list_json_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    page_number = request.GET.get("page", 1)
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list(page_number)

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list, "page": page_number}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        is_taken = False
        friend_list = Friend.objects.all()
        for friend in friend_list:
            if npm == friend.npm:
                is_taken = True
        if is_taken:
            return HttpResponse(None)
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    friend_id = request.POST.get('id', None)
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponse()

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    is_taken = False
    friend_list = Friend.objects.all()
    for friend in friend_list:
        if npm == friend.npm:
            is_taken = True

    data = {
        'is_taken': is_taken
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def friend_list_json(request):
    if request.method == 'GET':
        friend_list = Friend.objects.all()
        data = serializers.serialize('json', friend_list)
        return HttpResponse(data)
// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '1609851192427989',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus(function(response){
    render(response.status === 'connected');
  });

};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="profile container-fluid bg-overlay">' +
          '<img class="picture" src="' + user.picture.data.url + '" alt="profpic"/>' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h2>' + user.about + '</h2>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
          '<button class="logout btn btn-default btn-sm" onclick="facebookLogout()">Logout</button>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus btn btn-default btn-sm btn-facebook" onclick="postStatus()">Post ke Facebook</button>'
      );
      $('.bg-overlay').css('background','linear-gradient(rgba(0,0,0,.7), rgba(0,0,0,.7)), url("'+ user.cover.source + '")');

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          html = ''
          if (value.message || value.story){
            html+='<div class="list-group-item clearfix">';
            if (value.message)
              html+='<div>' + value.message + '</div>';
            if (value.story)
              html+='<div>' + value.story + '</div>';
            html+='<button type="button" class="pull-right btn btn-xs btn-danger"' +
              'onClick="deletePost(\'' + value.id + '\')">' +
              '&times; Remove Post' +
              '</button>' +
              '</div>';
          }
          $('#lab8').append(html)
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="btn btn-facebook btn-lg" onclick="facebookLogin()"><i class="fa fa-facebook fa-fw"></i> Login with Facebook</button>');
  }
};

const facebookLogin = () => {
  FB.login(function(response){
    render(response.status === 'connected');
  },{scope:'public_profile,user_about_me,email,user_posts,publish_actions'});
};

const facebookLogout = () => {
  FB.getLoginStatus(function(response){
    if (response.status === 'connected'){
      FB.logout();
      render(false);
    }else{
      render(false);
    }
  });
};

const getUserData = (fun) => {
  FB.getLoginStatus(function(response){
    if (response.status === 'connected'){
      FB.api('/me/', {fields:'name,about,email,gender,cover,picture'}, function (response){
        fun(response);
      });
    }else{
      render(false);
    }
  });
};

const getUserFeed = (fun) => {
  FB.getLoginStatus(function(response){
    if (response.status === 'connected'){
      FB.api('/me/feed/', 'GET', function (response){
        fun(response);
      });
    }else{
      render(false);
    }
  });
};

const postFeed = (message) => {
  FB.api('/me/feed', 'post', { message: message}, function(response){
    if (response && !response.error)
      render(true);
    else{
      alert('Gagal pos');
      render(false);
    }
  });
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
};

const deletePost = (postId) => {
  FB.api(postId, 'delete', function(response) {
    if (!response || response.error) {
      alert('Error occured');
    } else {
      render(true);
    }
  });
};
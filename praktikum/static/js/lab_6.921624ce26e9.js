//Chat
$('.chat-text').keypress(function(e){
    var key = e.which;
    if (key == 13) {
        e.preventDefault();
        var text = $('.chat-text textarea').val();
        $('.chat-text textarea').val('');
        $('.msg-insert').append('<div class=\"msg-send\">' + text + '</div>');
    }
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
    console.log('abc');
    console.log(print);
  if (x === 'ac') {
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//themes
var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.setItem('themes', JSON.stringify(themes));
var selectedTheme = JSON.parse(localStorage.getItem('themes'));
selectedTheme = selectedTheme[3];

mySelect = $('.my-select').select2();

mySelect.select2({
    'data': JSON.parse(localStorage.getItem('themes'))
});

if (localStorage.getItem('selectedTheme') !== null){
    selectedTheme = JSON.parse(localStorage.getItem('selectedTheme'));
}
else{
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
}

$('body').css(
    {
        'background-color': selectedTheme.bcgColor,
        'font-color': selectedTheme.fontColor
    }
)

$('.apply-button').on('click', function(){  // sesuaikan class button
    var selected = $('.my-select').select2().val();

    if (selected < themes.length)
        selectedTheme = JSON.parse(localStorage.getItem('themes'))[selected];

    $('body').css(
        {
            'background-color': selectedTheme.bcgColor,
            'font-color': selectedTheme.fontColor
        }
    )

    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
})

//chat toggle
$('#toggle-button').on('click', function(){
    $('.chat-body').toggle();
})